#!/usr/bin/env node

const prodRegex = /(?:v)(((\d+)\.\d+)\.\d+)$/;
const rcRegex = /(?:v)(((\d+)\.\d+\.\d+)-rc\d+)/;
let match;

[, ...args] = process.argv;

const version = args[1];
const minor = args[2] === '--minor' || args[2] === '-m';
const major = args[2] === '--major' || args[2] === '-M';

// if release candidate
if ((match = version.match(rcRegex))) {
  if (minor) {
    console.log(match[2] + '-rc');
  } else {
    console.log(match[1]);
  }
} else if ((match = version.match(prodRegex))) {
  if (major) {
    console.log(match[3]);
  } else if (minor) {
    console.log(match[2]);
  } else {
    console.log(match[1]);
  }
}

process.exit(0);
